#!/usr/bin/env bash

cd "$(dirname "$0")/.."

# Ensure that CHANGELOG.md does not contain duplicate versions.
DUPLICATE_CHANGELOG_VERSIONS=$(grep --extended-regexp '^## .+' CHANGELOG.md | sed -E 's| \(.+\)||' | sort -r | uniq -d)
if [ "${DUPLICATE_CHANGELOG_VERSIONS}" != "" ]
then
  echo '✖ ERROR: Duplicate versions in CHANGELOG.md:' >&2
  echo "${DUPLICATE_CHANGELOG_VERSIONS}" >&2
  exit 1
fi

# Checks only for stable branches
if [[ $CI_BUILD_REF_SLUG =~ ^[[:digit:]-]+-stable$ ]]
then

  echo "➤ Performing additional tests for stable branches..."

  # Ensure that release date has been updated in CHANGELOG.md.
  RELEASE_DATE=$(grep --perl-regexp --color=auto '## [[:digit:].]+ \(unreleased\)' CHANGELOG.md)
  if [ "${RELEASE_DATE}" != "" ]
  then
    echo '✖ ERROR: Need to have a valid release date for stable branches in CHANGELOG.md:' >&2
    echo "${RELEASE_DATE}" >&2
    exit 1
  fi

fi

echo "✔ Linting passed"
exit 0
