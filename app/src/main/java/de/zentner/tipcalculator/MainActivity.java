package de.zentner.tipcalculator;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements OnClickListener, TextWatcher {

  private EditText _billEditText;

  private TextView _tipTextView;
  private TextView _numberOfPeopleTextView;
  private TextView _totalPerPerson;

  private int _tipPercent = 20;
  private int _people = 2;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    init();
  }

  private void init(){
    try {
      ImageButton _addTipButton = findViewById(R.id.addTipButton);
      ImageButton _substractTipButton = findViewById(R.id.subtractTipButton);
      ImageButton _addPeopleButton = findViewById(R.id.addPeopleButton);
      ImageButton _substractPeopleButton = findViewById(R.id.subtractPeopleButton);

      _billEditText = findViewById(R.id.billEditText);

      _tipTextView = findViewById(R.id.tipTextView);
      _numberOfPeopleTextView = findViewById(R.id.numberOfPeopleTextView);
      _totalPerPerson = findViewById(R.id.totalPerPersonTextView);

      _addTipButton.setOnClickListener(this);
      _substractTipButton.setOnClickListener(this);
      _addPeopleButton.setOnClickListener(this);
      _substractPeopleButton.setOnClickListener(this);

      _billEditText.addTextChangedListener(this);
      _billEditText.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(5,2)});
    } catch (Exception e) {
      Log.e(AppGlobal.TAG, "Fail init View: ", e);
    }
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()){
      case R.id.addPeopleButton: incrementPeople(); break;
      case R.id.subtractPeopleButton: decrementPeople(); break;
      case R.id.addTipButton: incrementTip(); break;
      case R.id.subtractTipButton: decrementTip(); break;
    }
  }

  private void decrementTip() {
    try {
      if(_tipPercent > AppGlobal.MIN_TIP){
        _tipPercent--;
        _tipTextView.setText(String.format(Locale.getDefault(),"%d",_tipPercent));
      }
      calculateTip();
    } catch (Exception e) {
      Log.e(AppGlobal.TAG, "Fail decrement Tip: ", e);
    }
  }

  private void incrementTip() {
    try {
      _tipPercent++;
      _tipTextView.setText(String.format(Locale.getDefault(), "%d",_tipPercent));
      calculateTip();
    } catch (Exception e) {
      Log.e(AppGlobal.TAG, "Fail increment Tip: ", e);
    }
  }

  private void decrementPeople() {
    try {
      if(_people > AppGlobal.MIN_PERSON){
        _people--;
        _numberOfPeopleTextView.setText(String.format(Locale.getDefault(), "%d",_people));
      }
      calculateTip();
    } catch (Exception e) {
      Log.e(AppGlobal.TAG, "Fail decrement number of person: ", e);
    }
  }

  private void incrementPeople() {
    try {
      _people++;
      _numberOfPeopleTextView.setText(String.format(Locale.getDefault(), "%d",_people));
      calculateTip();
    } catch (Exception e) {
      Log.e(AppGlobal.TAG, "Fail increment number of person: ", e);
    }
  }

  private void calculateTip(){
    try {
      double total = Double.parseDouble(_billEditText.getText().toString());

      double completeTotal =
          ((AppGlobal.HUNDRET_PERCENT + Integer.parseInt(_tipTextView.getText().toString())) / AppGlobal.HUNDRET_PERCENT * total);

      double totalPerPerson = completeTotal / Integer.parseInt(_numberOfPeopleTextView.getText().toString());

      _totalPerPerson.setText(String.format(Locale.getDefault(), "%.2f", totalPerPerson));
    } catch (NumberFormatException e) {
      Log.e(AppGlobal.TAG, "Number Fail in calculate Tip: ", e);
    } catch (Exception e){
      Log.e(AppGlobal.TAG, "Fail calculate Tip: ", e);
    }

  }

  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    try {
      if(!_billEditText.getText().toString().isEmpty())
        calculateTip();
    } catch (Exception e) {
      Log.e(AppGlobal.TAG, "Fail in text change: ", e);
    }
  }

  @Override
  public void afterTextChanged(Editable editable) {

  }

  public class DecimalDigitsInputFilter implements InputFilter {

    Pattern mPattern;

    DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
      try {
        mPattern= Pattern
            .compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
      } catch (Exception e) {
        Log.e(AppGlobal.TAG, "Fail digit Filter: ", e);
      }
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

      try {
        Matcher matcher=mPattern.matcher(dest);
        if(!matcher.matches())
          return "";
      } catch (Exception e) {
        Log.e(AppGlobal.TAG, "Fail filter sequence: ", e);
      }
      return null;
    }

  }
}
