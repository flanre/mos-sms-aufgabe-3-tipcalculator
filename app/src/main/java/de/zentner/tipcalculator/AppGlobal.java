package de.zentner.tipcalculator;

public class AppGlobal {
  public static final String TAG = "TipCalculator";
  static final double HUNDRET_PERCENT = 100.00;
  static final int MIN_TIP  = 0;
  static final int MIN_PERSON = 1;
}
